{-# LANGUAGE OverloadedStrings #-}

module Main where

import Web.Scotty
import Web.Scotty.Internal.Types (ActionT)
import Data.Aeson
import Data.Text.Lazy
import GHC.Int
import Control.Monad.IO.Class


import Controller 
import View
import Model
import Route


main = scotty 3000 $ do

 ----  	  Pages     ----
 ------------------------
 -- route to Start Page
 get "/" $ do
         html $ pack (View.start)
 ------------------------
 ------------------------
 
 
 ----    CSS / JS    ----
 ------------------------
 -- makes javascript- and css- Files visible to client
 
 -- Client Library for calling the REST-API
 get "/static/restClient.js" $ do
         file $ "static/restClient.js"
 
 -- TODO add CSS for beauty
 
 -- Bootstrap
 get "/static/bootstrap.min.css" $ do
         file $ "static/bootstrap.min.css"
 
 -- GuitarStyles
 get "/static/guitarStyling.css" $ do
         file $ "static/guitarStyling.css"
 ------------------------
 ------------------------
 
 
 ---- REST-API Calls ----
 ------------------------
 -- Select GuitarTabs
 get "/select" $ do
         guitarTabs <- liftIO $ select_GuitarTabs
         Web.Scotty.json $ guitarTabs
 
 -- Create GuitarTab
 post "/create" $ do
         guitarTab <- jsonData :: ActionM (Maybe GuitarTab)
         result <- create_GuitarTab guitarTab
         if (0 < result)
                 then html $ pack (View.route Route.Create (guitarTab))
                 else html $ pack (View.route Route.CreateError (guitarTab))
 
 -- Update GuitarTab
 put "/update" $ do
         guitarTab <- jsonData :: ActionM (Maybe GuitarTab)
         html $ pack (View.route Route.Update guitarTab) 
 
 -- Delete GuitarTab
 delete "/delete" $ do 
         guitarTab <- jsonData :: ActionM (Maybe GuitarTab)
         html $ pack (View.route Route.Delete guitarTab)
 ------------------------
 ------------------------
 
 
 ----  404 not found ----
 ------------------------
 -- the notFound-Route must be at the bottom
 -- if its at top it will block all routes
 
 -- TODO redirect to 404-Page
 notFound $ do
         html $ pack (View.start)
 ------------------------
 ------------------------
