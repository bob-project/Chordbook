# Changelog for Chordbook
Version 0.1
- DONE verschiede Routing-Methoden schreiben
- DONE in der View gibt es noch Bugs, er routet nicht auf die Create-Route sondern nutzt die im Fehlerfall-Route
- DONE über Post: json-Objekt-Weitergabe an die GUI implementieren
- DONE Routing-Code generalisieren
- DONE Client-Seiten - die über javascript Post-Anfragen versenden
- DONE was macht man jetzt mit der Antwort vom Server?!? -> kann man damit die aktuelle Seite aktualisieren?!?
- DONE Die Funktionalität limitiren und für den Anfang nur 5 Gitarrenbünde anzeigen
- DONE in Html: Chord-Table schreiben, in Javascript: eine Funktion schreiben, die den Table ausliest und ein JSON-Objekt daraus erstellt
- DONE Hamlet: shakespeare-Template nutzen um die verschiedenen Views über 1 parametrisiertes Template darzustellen
- DONE Datenbank-Funktionen schreiben und an den Controller anknüpfen -- Insert und Select
- DONE Datenbank-Funktionen generalisieren
- DONE json-Objekt-Weitergabe an die Datenbank implementieren
- DONE Debuggen: warum wird das JSON-Objekt falsch übertragen? 
- DONE Javascript-Code generalisieren
- DONE Controller generalisiert
- DONE alle angezeigten String in eine seperate Klasse -- zu erst nur aus der View-Klasse
- DONE einheitliche Kommentare und Sections
- DONE Postgres: neue Tabelle für Griffarten, eine Spalte bzw. weitere Tabelle für Griffshapes, mehrere Griffarten können einem Akkord zugeordnet sein, der Index (Griffshape + Fret)
- DONE Haskell: neues Objekt für neue Tabelle erfordert Anpassung in der Application selbst
- DONE Javascript/CSS: Die schwarzen Balken(Checkbox) sollen sich bei größeren/kleineren Spalten dynamisch verbreitern/verkleinern 
- DONE CSS: Stil angepasst

## Unreleased changes
