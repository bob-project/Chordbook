////  init Combobox ////
////////////////////////
window.onload = sendSelectGuitarTabs();
function sendSelectGuitarTabs () {

	var Xhr = initialiseXHR("GET", "/select");
	Xhr.onreadystatechange = function () {
	    if (Xhr.readyState === 4 && Xhr.status === 200) {
		    fillGuitarTabComboBox(JSON.parse(Xhr.responseText));
	    }
	};

	Xhr.send();
}

function initialiseXHR (request, url) {
	var Xhr = new XMLHttpRequest();
	Xhr.open(request, url, true);
	Xhr.setRequestHeader("Content-Type", "application/json");
	return Xhr;
}

function fillGuitarTabComboBox (guitarTabs) {

	var comboBox = document.getElementById("guitarTabComboBox");
	for (var chordIndex in guitarTabs)
	{
		var chord = guitarTabs[chordIndex].chord;
		var option = document.createElement("option");
		option.id = chord;
		option.value = chord;
		option.innerHTML = chord;
		comboBox.appendChild(option);
	}
}
////////////////////////
////////////////////////

//// OnChangeComboBox////
////////////////////////
function reactOnChange () {
	var selection = document.getElementById('guitarTabComboBox');
	var option = getSelectedOption(selection);
}

function getSelectedOption(selection) {
	var opt;
	for (var selIndex= 0, selLength = selection.options.length; selIndex < selLength; selIndex++ ) {

		opt = selection.options[selIndex];
		if ( opt.selected === true ) {
			break;
		}
	}
	return opt;
} 
////////////////////////
////////////////////////

////  Btn-Requests  ////
////////////////////////
function sendPostCreate () {
	sendRequest("POST", "/create");
}

function sendPutUpdate () {
	sendRequest("PUT", "/update");
}

function sendDelete () {
	sendRequest("DELETE", "/delete");
}

function sendRequest (request, url) {

	var Xhr = initialiseXHR(request, url);
	Xhr.onreadystatechange = function () {
	    if (Xhr.readyState === 4 && Xhr.status === 200) {
		    refreshCurrentPage(Xhr.responseText);
	    }
	};

	var Data = getCurrentGuitarTab();
	Xhr.send(Data);
}

function refreshCurrentPage (newHtml) {
	
	document.open();
	document.write(newHtml);
	document.close();
}
////////////////////////
////////////////////////


//// getCurGuitarTab ////
////////////////////////
function getCurrentGuitarTab () {

	var extractedValues = extractNotes();
	return mapGuitarTab(extractedValues);
}

function extractNotes() {

	var extractedNotes = [];
	var table = document.getElementById("fretboardTable");
	var totalColumns = table.rows[0].cells.length;

	for (var colIndex = 1; colIndex < totalColumns; colIndex++) {

		for (var rowIndex = 1, row; row = table.rows[rowIndex]; rowIndex++) {

			var stringIndex = 7 - rowIndex;
			var stringName = row.cells[0].textContent;
			var col = row.cells[colIndex];

			var inputBox = col.firstChild
			if (true == inputBox.checked) {
				extractedNotes.push(colIndex + "-" + stringIndex);
			}
		}
	}

	return extractedNotes;
}

function mapGuitarTab (extractedValues) {

	var guitarTab = JSON.stringify({
		"guitartab_id": null,
		"finger1": extractedValues[0] || null, 
		"finger2": extractedValues[1] || null,
		"finger3": extractedValues[2] || null,
		"finger4": extractedValues[3] || "0-0",
		"thumb": null,
		"barre": null, 
		"shape": null,
		"chord": null 
	});
	return guitarTab;
}
////////////////////////
///////////////////////
