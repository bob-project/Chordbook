{-# LANGUAGE OverloadedStrings #-}

module ChordSpec
  where

import Test.Hspec
import Chord
import Position
import Model

spec :: Spec
spec = do

  describe "orderPositionsByLowestY" $ do
    it "test if descending is ordered to ascending" $
      (orderPositionsByLowestY positionList_desc) `shouldBe` positionList_asc

    it "test if ascending is ordered to ascending" $
      (orderPositionsByLowestY positionList_asc) `shouldBe` positionList_asc

    it "test if mix1 is ordered to ascending" $
      (orderPositionsByLowestY positionList_mix1) `shouldBe` positionList_asc

    it "test if mix2 is ordered to ascending" $
      (orderPositionsByLowestY positionList_mix2) `shouldBe` positionList_asc

    it "test if mix3 is ordered to ascending" $
      (orderPositionsByLowestY positionList_mix3) `shouldBe` positionList_asc

    it "test if mix4 is ordered to ascending" $
      (orderPositionsByLowestY positionList_mix4) `shouldBe` positionList_asc
  
    it "test if [] is ordered to []" $
      (orderPositionsByLowestY positionList_mix4) `shouldBe` positionList_asc

  describe "createPositionList" $ do
    it "test if a non empty positionList is created" $
      (createPositionList guitarTab_e) `shouldBe`
        [ Position 1 3
        , Position 2 5
        , Position 2 4 ]

  describe "filterPositionsByY" $ do
    let positionList = createPositionList guitarTab_e 
    let filteredList = filterPositionsByY positionList
    it "test if a positionList is filtered correctly" $
      filteredList`shouldBe`
        [ Position 1 3
        , Position 2 5
        , Position 2 4 ]

  describe "fillWithOpenPositions" $ do
    let positionList = createPositionList guitarTab_e 
    let filteredList = filterPositionsByY positionList
    let updatedWithOpenChordsList = (createOpenPositions filteredList openPositionsList) ++ filteredList
    it "test if a positionList is correctly filled with openPositions" $
      updatedWithOpenChordsList `shouldBe`
        [ Position 1 3
        , Position 2 5
        , Position 2 4 ]

  describe "orderPositionsByLowestY" $ do
    let positionList = createPositionList guitarTab_e 
    let filteredList = filterPositionsByY positionList
    let updatedWithOpenChordsList = (createOpenPositions filteredList openPositionsList) ++ filteredList
    let orderedList = orderPositionsByLowestY updatedWithOpenChordsList
    it "test if a positionList is correctly ordered" $
      orderedList `shouldBe`
        [ Position 1 3
        , Position 2 5
        , Position 2 4 ]

  describe "transformOrderedPositionsToChord" $ do
    let positionList = createPositionList guitarTab_e 
    let filteredList = filterPositionsByY positionList
    let updatedWithOpenChordsList = (createOpenPositions filteredList openPositionsList) ++ filteredList
    let orderedList = orderPositionsByLowestY updatedWithOpenChordsList
    it "test if a positionList is correctly transformed to a Chord" $
      transformOrderedPositionsToChord orderedList `shouldBe` chord_e

  describe "convertGuitarTabToChord" $ do
    it "test if an e is converted correctly" $
      (convertGuitarTabToChord guitarTab_e) `shouldBe` chord_e

  describe "toNote" $ do
    it "test if (toNote 0) is A " $
      toNote (0 :: Int) `shouldBe` A
    it "test if (toNote 1) is AB " $
      toNote (1 :: Int) `shouldBe` AB
    it "test if (toNote 2) is B " $
      toNote (2 :: Int) `shouldBe` B
    it "test if (toNote 3) is C " $
      toNote (3 :: Int) `shouldBe` C
    it "test if (toNote 4) is CD " $
      toNote (4 :: Int) `shouldBe` CD
    it "test if (toNote 5) is D " $
      toNote (5 :: Int) `shouldBe` D
    it "test if (toNote 6) is DE " $
      toNote (6 :: Int) `shouldBe` DE
    it "test if (toNote 7) is E " $
      toNote (7 :: Int) `shouldBe` E
    it "test if (toNote 8) is F " $
      toNote (8 :: Int) `shouldBe` F
    it "test if (toNote 9) is FG " $
      toNote (9 :: Int) `shouldBe` FG
    it "test if (toNote 10) is G " $
      toNote (10 :: Int) `shouldBe` G
    it "test if (toNote 11) is GA " $
      toNote (11 :: Int) `shouldBe` GA

    it "test if toNote (Position 13 1) is F" $
      toNote (Position 13 1) `shouldBe` F
    it "test if toNote (Position 13 2) is AB" $
      toNote (Position 13 2) `shouldBe` AB
    it "test if toNote (Position 13 3) is DE" $
      toNote (Position 13 3) `shouldBe` DE 
    it "test if toNote (Position 13 4) is GA" $
      toNote (Position 13 4) `shouldBe` GA 
    it "test if toNote (Position 13 5) is C" $
      toNote (Position 13 5) `shouldBe` C
    it "test if toNote (Position 13 6) is F" $
      toNote (Position 13 6) `shouldBe` F


positionList_asc :: [Position]
positionList_asc = [ Position 1 1
               , Position 2 2
               , Position 3 3
               ]

positionList_desc :: [Position]
positionList_desc = [ Position 3 3
               , Position 2 2
               , Position 1 1
               ]

positionList_mix1 :: [Position]
positionList_mix1 = [ Position 2 2
               , Position 3 3
               , Position 1 1
               ]

positionList_mix2 :: [Position]
positionList_mix2 = [ Position 1 1
               , Position 3 3
               , Position 2 2
               ]

positionList_mix3 :: [Position]
positionList_mix3 = [ Position 3 3
               , Position 1 1
               , Position 2 2
               ]

positionList_mix4 :: [Position]
positionList_mix4 = [ Position 2 2
               , Position 1 1
               , Position 3 3
               ]

guitarTab_e = GuitarTab {
  finger1 = "1-3",
  finger2 = "2-5",
  finger3 = "2-4",
  finger4 = "",
  thumb = Nothing
}

chord_e = Chord {
  chord_id = Nothing,
  tabshape_chord_id = Nothing,
  note_1 = "E",
  note_2 = "B",
  note_3 = "E",
  note_4 = "G#/Ad",
  note_5 = "",
  note_6 = "",
  note_7 = "",
  note_8 = "",
  note_9 = "",
  note_10 = "",
  numberOfNotes = "4",
  inversion = "",
  scaleDegree = "",
  symbol = ""
} 
