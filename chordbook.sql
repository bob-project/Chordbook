--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: determine_chordbynotes(character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION determine_chordbynotes(p_finger1 character varying, p_finger2 character varying, p_finger3 character varying, OUT p_rootnote character varying, OUT p_scndnote character varying, OUT p_thrdnote character varying, OUT p_chordtype character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
v_outerRecord	record;
v_innerRecord	record;
v_centralRecord	record;
v_rootNote character varying;
v_scndNote character varying;
v_thrdNote character varying;
v_shouldBeNextNote character varying;
v_count	int;
BEGIN

   p_rootNote := '';
   p_scndNote := '';
   p_thrdNote := '';
   p_chordType := '';

   -- 1. gespielte Positionen die über Barre und Finger dargestellt werden -- (Barre wird noch nicht berücksichtigt)
   -- 2. Positionen in Töne umwandeln und abspeichern
   -- 3. Lösche alle Duplikate aus dem Array
   -- 4. Dur (3-Töne-Suche)
   v_count := COUNT(DISTINCT(note)) as note from (
		SELECT translate_CoordinateToNote(p_finger1)
		UNION
		SELECT translate_CoordinateToNote(p_finger2)
		UNION
		SELECT translate_CoordinateToNote(p_finger3)
		UNION
		SELECT translate_CoordinateToNote('12-' || subQuery.string) from (
			SELECT distinct(string) as string from coordinate where string NOT IN (
			SELECT string from coordinate where coordinate_id = p_finger1
			OR coordinate_id = p_finger2
			OR coordinate_id = p_finger3)) as subQuery
		) as note; 

   IF (3 <> v_count) THEN
	return;
   END IF;

   FOR v_outerRecord IN (
	   SELECT DISTINCT(note) as note from (
		SELECT translate_CoordinateToNote(p_finger1)
		UNION
		SELECT translate_CoordinateToNote(p_finger2)
		UNION
		SELECT translate_CoordinateToNote(p_finger3)
		UNION
		SELECT translate_CoordinateToNote('12-' || subQuery.string) from (
			SELECT distinct(string) as string from coordinate where string NOT IN (
			SELECT string from coordinate where coordinate_id = p_finger1
			OR coordinate_id = p_finger2
			OR coordinate_id = p_finger3)) as subQuery
		) as note
   )
   LOOP
	
	   v_rootNote := v_outerRecord.note;  
	   -- Die Select-Query vorher sorgt für eine Umklammerung des note-Values, deswegen wird hier ein regexp benutzt um die Klammerung aufzuheben
	   SELECT SUBSTRING(v_rootNote from '[0-9a-zA-Z#/]+') into v_rootNote;
	   v_shouldBeNextNote := find_NextNote(v_rootNote, 4);
	   SELECT SUBSTRING(v_shouldBeNextNote from '[0-9a-zA-Z#/]+') into v_shouldBeNextNote;
		
	   FOR v_innerRecord IN (
		   SELECT DISTINCT(note) from (
			SELECT translate_CoordinateToNote(p_finger1)
			UNION
			SELECT translate_CoordinateToNote(p_finger2)
			UNION
			SELECT translate_CoordinateToNote(p_finger3)
			UNION
			SELECT translate_CoordinateToNote('12-' || subQuery.string) from (
				SELECT distinct(string) as string from coordinate where string NOT IN (
				SELECT string from coordinate where coordinate_id = p_finger1
				OR coordinate_id = p_finger2
				OR coordinate_id = p_finger3)) as subQuery
			) as note
	   )
	   LOOP

		v_scndNote = v_innerRecord.note;
	   	SELECT SUBSTRING(v_scndNote from '[0-9a-zA-Z#/]+') into v_scndNote;
		IF (v_shouldBeNextNote = v_scndNote) THEN
		
	   		v_shouldBeNextNote := find_NextNote(v_scndNote, 3);
	   		SELECT SUBSTRING(v_shouldBeNextNote from '[0-9a-zA-Z#/]+') into v_shouldBeNextNote;

			   FOR v_centralRecord IN (
				   SELECT DISTINCT(note) from (
					SELECT translate_CoordinateToNote(p_finger1)
					UNION
					SELECT translate_CoordinateToNote(p_finger2)
					UNION
					SELECT translate_CoordinateToNote(p_finger3)
					UNION
					SELECT translate_CoordinateToNote('12-' || subQuery.string) as note from (
						SELECT distinct(string) as string from coordinate where string NOT IN (
						SELECT string from coordinate where coordinate_id = p_finger1
						OR coordinate_id = p_finger2
						OR coordinate_id = p_finger3)) as subQuery
					) as note
			   )
			   LOOP
				
				v_thrdNote := v_centralRecord.note;
	   			SELECT SUBSTRING(v_thrdNote from '[0-9a-zA-Z#/]+') into v_thrdNote;
				IF (v_shouldBeNextNote = v_thrdNote) THEN

					p_rootNote := v_rootNote;
					p_scndNote := v_scndNote;
					p_thrdNote := v_thrdNote;
					p_chordType := 'Dur';
					RETURN NEXT;
				END IF;	   
			   END LOOP;
		END IF;
	   END LOOP; 
   END LOOP;

   -- TODO Schreibe eine Mini-Methode für jeden Akkord-Typ
   -- TODO 4 Töne Suche nach Septim-Akkorden
END;
$$;


ALTER FUNCTION public.determine_chordbynotes(p_finger1 character varying, p_finger2 character varying, p_finger3 character varying, OUT p_rootnote character varying, OUT p_scndnote character varying, OUT p_thrdnote character varying, OUT p_chordtype character varying) OWNER TO postgres;

--
-- Name: determine_chordbynotes(character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION determine_chordbynotes(p_finger1 character varying, p_finger2 character varying, p_finger3 character varying, p_finger4 character varying, OUT p_rootnote character varying, OUT p_scndnote character varying, OUT p_thrdnote character varying, OUT p_chordtype character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
v_outerRecord	record;
v_innerRecord	record;
v_centralRecord	record;
v_rootNote character varying;
v_scndNote character varying;
v_thrdNote character varying;
v_shouldBeNextNote character varying;
v_count	int;
BEGIN

   p_rootNote := '';
   p_scndNote := '';
   p_thrdNote := '';
   p_chordType := '';

   -- 1. gespielte Positionen die über Barre und Finger dargestellt werden -- (Barre wird noch nicht berücksichtigt)
   -- 2. Positionen in Töne umwandeln und abspeichern
   -- 3. Lösche alle Duplikate aus dem Array
   -- 4. Dur (3-Töne-Suche)
   v_count := COUNT(DISTINCT(note)) as note from (
	SELECT translate_CoordinateToNote(p_finger1) as note
		UNION
		SELECT translate_CoordinateToNote(p_finger2) as note
		UNION
		SELECT translate_CoordinateToNote(p_finger3) as note
		UNION
		SELECT translate_CoordinateToNote('12-' || subQuery.string) as note from (
			SELECT distinct(string) as string from coordinate where string NOT IN (
			SELECT string from coordinate where coordinate_id = p_finger1
			OR coordinate_id = p_finger2
			OR coordinate_id = p_finger3)) as subQuery
		) as note where note <> '';

   IF (3 <> v_count) THEN
	return;
   END IF;

   FOR v_outerRecord IN (
	   SELECT DISTINCT(note) as note from (
		SELECT translate_CoordinateToNote(p_finger1) as note
		UNION
		SELECT translate_CoordinateToNote(p_finger2) as note
		UNION
		SELECT translate_CoordinateToNote(p_finger3) as note
		UNION
		SELECT translate_CoordinateToNote('12-' || subQuery.string) as note from (
			SELECT distinct(string) as string from coordinate where string NOT IN (
			SELECT string from coordinate where coordinate_id = p_finger1
			OR coordinate_id = p_finger2
			OR coordinate_id = p_finger3)) as subQuery
	   ) as note where note <> ''
   )
   LOOP
	
	   v_rootNote := v_outerRecord.note;  
	   -- Die Select-Query vorher sorgt für eine Umklammerung des note-Values, deswegen wird hier ein regexp benutzt um die Klammerung aufzuheben
	   SELECT SUBSTRING(v_rootNote from '[0-9a-zA-Z#/]+') into v_rootNote;
	   v_shouldBeNextNote := find_NextNote(v_rootNote, 4);
	   SELECT SUBSTRING(v_shouldBeNextNote from '[0-9a-zA-Z#/]+') into v_shouldBeNextNote;
		
	   FOR v_innerRecord IN (
		   SELECT DISTINCT(note) from (
			SELECT translate_CoordinateToNote(p_finger1) as note
			UNION
			SELECT translate_CoordinateToNote(p_finger2) as note
			UNION
			SELECT translate_CoordinateToNote(p_finger3) as note
			UNION
			SELECT translate_CoordinateToNote('12-' || subQuery.string) as note from (
				SELECT distinct(string) as string from coordinate where string NOT IN (
				SELECT string from coordinate where coordinate_id = p_finger1
				OR coordinate_id = p_finger2
				OR coordinate_id = p_finger3)) as subQuery
			) as note where note <> ''
	   )
	   LOOP

		v_scndNote = v_innerRecord.note;
	   	SELECT SUBSTRING(v_scndNote from '[0-9a-zA-Z#/]+') into v_scndNote;
		IF (v_shouldBeNextNote = v_scndNote) THEN
		
	   		v_shouldBeNextNote := find_NextNote(v_scndNote, 3);
	   		SELECT SUBSTRING(v_shouldBeNextNote from '[0-9a-zA-Z#/]+') into v_shouldBeNextNote;

			   FOR v_centralRecord IN (
				   SELECT DISTINCT(note) from (
					SELECT translate_CoordinateToNote(p_finger1) as note
					UNION
					SELECT translate_CoordinateToNote(p_finger2) as note
					UNION
					SELECT translate_CoordinateToNote(p_finger3) as note
					UNION
					SELECT translate_CoordinateToNote('12-' || subQuery.string) as note from (
						SELECT distinct(string) as string from coordinate where string NOT IN (
						SELECT string from coordinate where coordinate_id = p_finger1
						OR coordinate_id = p_finger2
						OR coordinate_id = p_finger3)) as subQuery
		  			) as note where note <> ''
			   )
			   LOOP
				
				v_thrdNote := v_centralRecord.note;
	   			SELECT SUBSTRING(v_thrdNote from '[0-9a-zA-Z#/]+') into v_thrdNote;
				IF (v_shouldBeNextNote = v_thrdNote) THEN

					p_rootNote := v_rootNote;
					p_scndNote := v_scndNote;
					p_thrdNote := v_thrdNote;
					p_chordType := 'Dur';
					RETURN NEXT;
				END IF;	   
			   END LOOP;
		END IF;
	   END LOOP; 
   END LOOP;

   -- TODO Schreibe eine Mini-Methode für jeden Akkord-Typ
   -- TODO 4 Töne Suche nach Septim-Akkorden
END;
$$;


ALTER FUNCTION public.determine_chordbynotes(p_finger1 character varying, p_finger2 character varying, p_finger3 character varying, p_finger4 character varying, OUT p_rootnote character varying, OUT p_scndnote character varying, OUT p_thrdnote character varying, OUT p_chordtype character varying) OWNER TO postgres;

--
-- Name: find_guitartabshape(character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION find_guitartabshape(p_finger1 character varying, p_finger2 character varying, p_finger3 character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
v_frstFret	int;
v_frstString	int;
v_scndFret	int;
v_scndString	int;
v_thrdFret	int;
v_thrdString	int;
BEGIN

   select fret, string from coordinate where coordinate_id = p_finger1 into v_frstFret, v_frstString;
   select fret, string from coordinate where coordinate_id = p_finger2 into v_scndFret, v_scndString;
   select fret, string from coordinate where coordinate_id = p_finger3 into v_thrdFret, v_thrdString;

   IF (v_frstFret = v_scndFret) THEN

	IF (v_scndFret = v_thrdFret 
	AND 4 = v_frstString
	AND 3 = v_scndString
	AND 2 = v_thrdString)
	THEN 
		RETURN 'A-Shape';

	
	ELSIF ((1 + v_frstFret) = v_thrdFret
	AND 3 = v_frstString
	AND 1 = v_scndString
	AND 2 = v_thrdString)
	THEN
		RETURN 'D-Shape';
	END IF;

   
   ELSIF ((1 + v_frstFret) = v_scndFret) THEN  

	IF ((2 + v_frstFret) = v_thrdFret
	AND 2 = v_frstString
	AND 4 = v_scndString
	AND 5 = v_thrdString)
	THEN
		RETURN 'C-Shape';


	ELSIF (v_scndFret = v_thrdFret) THEN

		IF (5 = v_frstString
		AND 6 = v_scndString
		AND 1 = v_thrdString)
		THEN
			RETURN 'G-Shape';
		

		ELSIF (3 = v_frstString
		AND 5 = v_scndString
		AND 4 = v_thrdString)
		THEN
			RETURN 'E-Shape';
		END IF;
	END IF;
   END IF;

   RETURN 'Undefined';
END;
$$;


ALTER FUNCTION public.find_guitartabshape(p_finger1 character varying, p_finger2 character varying, p_finger3 character varying) OWNER TO postgres;

--
-- Name: find_nextnote(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION find_nextnote(p_notename character varying, p_semitones integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
v_fret	int;
v_relativePosition	int;
v_distanceToEnd	int;
v_totalFrets	int;
v_finalIndex	int;
v_noteName	character varying;
BEGIN 

   SELECT num FROM (
	VALUES 
		(1, 'A'), (2, 'A#/Bd'), (3, 'B'),
		(4, 'C'), (5, 'C#/Dd'), (6, 'D'),
		(7, 'D#/Ed'), (8, 'E'), (9, 'F'),
		(10, 'F#/Gd'), (11, 'G'), (12, 'G#/Ad')
   ) AS tones(num, name)
   where name = p_noteName into v_relativePosition;

   v_fret := p_semitones;
   v_distanceToEnd := 12 - v_relativePosition;

   -- 1. Die Distanz bis zum Ende von den totalen Frets abziehen
   v_totalFrets := v_fret - v_distanceToEnd;

   -- 2A. Wenn totale Frets <= 0, dann können wir schon zur Note navigieren
   IF (0 >= v_totalFrets) THEN
	v_finalIndex := v_relativePosition + v_fret;

   -- 2B. totale Frets immernoch > 0
   ---  dann -> verbliebene Fretanzahl durch Modulo 12 teilen
   ---  Rest nehmen und von 0 ausgehend die Note auswählen
   ELSE
   	v_finalIndex = v_totalFrets % 12;
   END IF;

   SELECT name FROM (
	VALUES 
		(1, 'A'), (2, 'A#/Bd'), (3, 'B'),
		(4, 'C'), (5, 'C#/Dd'), (6, 'D'),
		(7, 'D#/Ed'), (8, 'E'), (9, 'F'),
		(10, 'F#/Gd'), (11, 'G'), (12, 'G#/Ad')
   ) AS tones(num, name)
   where num = v_finalIndex into v_noteName;

   RETURN v_noteName; 
END;
$$;


ALTER FUNCTION public.find_nextnote(p_notename character varying, p_semitones integer) OWNER TO postgres;

--
-- Name: guitartab_bef_trg(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION guitartab_bef_trg() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE 
v_tabShape	character varying;
v_rootNote	character varying;
v_scndNote	character varying;
v_thrdNote	character varying;
v_chordType	character varying;
v_count	int;
BEGIN

  IF ('INSERT' = TG_OP 
  OR 'UPDATE' = TG_OP) 
  THEN
	
	-- Search for GuitarTab-Shape 
	--   start from note1-note3
	--     when Nothing was found then from note2-note4
	SELECT find_GuitarTabShape(new.finger1, new.finger2, new.finger3) INTO v_tabShape;

	IF ('Undefined' = v_tabShape) THEN
		SELECT find_GuitarTabShape(new.finger2, new.finger3, new.finger4) INTO v_tabShape;
	END IF;

	--   when already created link to it
	--   else create it
	--   Note: 
	--     - guitartab.barre is currently not supported
	--     - guitartab.thumb is currently not supported
	SELECT p_rootNote, p_scndNote, p_thrdNote, p_chordType 
	FROM determine_ChordByNotes(NEW.finger1, NEW.finger2, NEW.finger3, NEW.finger4)
	INTO v_rootNote, v_scndNote, v_thrdNote, v_chordType;

	v_count := COUNT(*) FROM chord where chord_id = v_rootNote;
	IF (1 > v_count) THEN
		INSERT INTO chord (chord_id, note1_id, note2_id, note3_id, chord_type_id) VALUES (v_rootNote, v_rootNote, v_scndNote, v_thrdNote, v_chordType);
	END IF;

	-- Give the Tab a Name
	NEW.guitartab_id := v_rootNote || ' ' || v_chordType;
 	NEW.shape := v_tabShape;
	NEW.chord := v_rootNote;
   END IF;
   RETURN NEW;
END;
$$;


ALTER FUNCTION public.guitartab_bef_trg() OWNER TO postgres;

--
-- Name: translate_coordinatetonote(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION translate_coordinatetonote(p_choordinate_id character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
v_fret	int;
v_string	int;
v_relativePosition	int;
v_distanceToEnd	int;
v_totalFrets	int;
v_finalIndex	int;
v_noteName	character varying;
BEGIN 
   select fret, string from coordinate where coordinate_id = p_choordinate_id into v_fret, v_string;

   IF (6 = v_string) THEN -- Map to E
	v_relativePosition := 8;	
   ELSIF (5 = v_string) THEN -- Map to A
	v_relativePosition := 1;	
   ELSIF (4 = v_string) THEN -- Map to D
	v_relativePosition := 6;	
   ELSIF (3 = v_string) THEN -- Map to G
	v_relativePosition := 11;	
   ELSIF (2 = v_string) THEN -- Map to B
	v_relativePosition := 3;	
   ELSIF (1 = v_string) THEN -- Map to e
	v_relativePosition := 8;	
   END IF;

   v_distanceToEnd := 12 - v_relativePosition;

   -- 1. Die Distanz bis zum Ende von den totalen Frets abziehen
   v_totalFrets := v_fret - v_distanceToEnd;

   -- 2A. Wenn totale Frets <= 0, dann können wir schon zur Note navigieren
   IF (0 >= v_totalFrets) THEN
	v_finalIndex := v_relativePosition + v_fret;

   -- 2B. totale Frets immernoch > 0
   ---  dann -> verbliebene Fretanzahl durch Modulo 12 teilen
   ---  Rest nehmen und von 0 ausgehend die Note auswählen
   ELSE
   	v_finalIndex = v_totalFrets % 12;
   END IF;
   SELECT name  FROM (
	VALUES 
		(1, 'A'), (2, 'A#/Bd'), (3, 'B'),
		(4, 'C'), (5, 'C#/Dd'), (6, 'D'),
		(7, 'D#/Ed'), (8, 'E'), (9, 'F'),
		(10, 'F#/Gd'), (11, 'G'), (12, 'G#/Ad')
   ) AS tones(num, name)
   where num = v_finalIndex into v_noteName;

   RETURN v_noteName; 
END;
$$;


ALTER FUNCTION public.translate_coordinatetonote(p_choordinate_id character varying) OWNER TO postgres;

--
-- Name: translate_coordinatetotone(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION translate_coordinatetotone(p_chord_id character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
v_fret	int;
v_string	int;
v_relativePosition	int;
v_distanceToEnd	int;
v_totalFrets	int;
v_finalIndex	int;
v_noteName	character varying;
BEGIN

   select fret, string from coordinate where coordinate_id = p_finger1 into v_fret, v_string;

   IF (6 = string) THEN
	v_relativePosition := 8;	
   ELSIF (5 = string) THEN
	v_relativePosition := 1;	
   ELSIF (4 = string) THEN
	v_relativePosition := 6;	
   ELSIF (3 = string) THEN
	v_relativePosition := 11;	
   ELSIF (2 = string) THEN
	v_relativePosition := 3;	
   ELSIF (1 = string) THEN
	v_relativePosition := 8;	
   END IF;

   v_distanceToEnd := 12 - v_relativePosition;

   -- 1. Die Distanz bis zum Ende von den totalen Frets abziehen
   -- 2A. Wenn totale Frets <= 0, dann können wir schon zur Note navigieren
   v_totalFrets := v_fret - v_distanceToEnd;
   IF (0 >= v_totalFrets) THEN
	v_finalIndex := v_relativePosition + v_distanceToEnd;

   -- 2B. totale Frets immernoch > 0
   ---  dann -> verbliebene Fretanzahl durch Modulo 12 teilen
   ---  Rest nehmen und von 0 ausgehend die Note auswählen
   ELSE
   	v_finalIndex = v_totalFrets % 12;
   END IF;

   SELECT name FROM (VALUES (1, 'A'), (2, 'A#/Bd'), (3, 'B'),
   (4, 'C'), (5, 'C#/Dd'), (6, 'D'),
   (7, 'D#/Ed'), (8, 'E'), (9, 'F'),
   (10, 'F#/Gd'), (11, 'G'), (12, 'G#/Ad')) AS tones(num, name)
   where num = v_finalIndex into v_noteName;

   RETURN v_noteName; 
END;
$$;


ALTER FUNCTION public.translate_coordinatetotone(p_chord_id character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: chord; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE chord (
    chord_id character varying(32) NOT NULL,
    chord_type_id character varying(32) NOT NULL,
    note1_id character varying(32) NOT NULL,
    note2_id character varying(32) NOT NULL,
    note3_id character varying(32) NOT NULL,
    note4_id character varying(32)
);


ALTER TABLE chord OWNER TO postgres;

--
-- Name: chord_ordering; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE chord_ordering (
    chord_ordering_id character varying(32) NOT NULL,
    value character varying(40) NOT NULL
);


ALTER TABLE chord_ordering OWNER TO postgres;

--
-- Name: chord_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE chord_type (
    chord_type_id character varying(32) NOT NULL
);


ALTER TABLE chord_type OWNER TO postgres;

--
-- Name: coordinate; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE coordinate (
    coordinate_id character varying(32) NOT NULL,
    fret integer NOT NULL,
    string integer NOT NULL
);


ALTER TABLE coordinate OWNER TO postgres;

--
-- Name: guitartab; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE guitartab (
    guitartab_id character varying(32) NOT NULL,
    finger1 character varying(32) NOT NULL,
    finger2 character varying(32) NOT NULL,
    finger3 character varying(32) NOT NULL,
    finger4 character varying(32) NOT NULL,
    thumb character varying(32),
    barre character varying(32),
    shape character varying(32) NOT NULL,
    chord character varying(32) NOT NULL
);


ALTER TABLE guitartab OWNER TO postgres;

--
-- Name: guitartab_shape; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE guitartab_shape (
    guitartab_shape_id character varying(32) NOT NULL
);


ALTER TABLE guitartab_shape OWNER TO postgres;

--
-- Name: note; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE note (
    note_id character varying(32) NOT NULL,
    value character varying(40) NOT NULL
);


ALTER TABLE note OWNER TO postgres;

--
-- Data for Name: chord; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY chord (chord_id, chord_type_id, note1_id, note2_id, note3_id, note4_id) FROM stdin;
\.


--
-- Data for Name: chord_ordering; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY chord_ordering (chord_ordering_id, value) FROM stdin;
\.


--
-- Data for Name: chord_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY chord_type (chord_type_id) FROM stdin;
Dur
\.


--
-- Data for Name: coordinate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY coordinate (coordinate_id, fret, string) FROM stdin;
1-1	1	1
2-1	2	1
3-1	3	1
4-1	4	1
5-1	5	1
6-1	6	1
7-1	7	1
8-1	8	1
9-1	9	1
10-1	10	1
11-1	11	1
12-1	12	1
13-1	13	1
14-1	14	1
15-1	15	1
16-1	16	1
17-1	17	1
18-1	18	1
19-1	19	1
20-1	20	1
21-1	21	1
22-1	22	1
23-1	23	1
24-1	24	1
1-2	1	2
2-2	2	2
3-2	3	2
4-2	4	2
5-2	5	2
6-2	6	2
7-2	7	2
8-2	8	2
9-2	9	2
10-2	10	2
11-2	11	2
12-2	12	2
13-2	13	2
14-2	14	2
15-2	15	2
16-2	16	2
17-2	17	2
18-2	18	2
19-2	19	2
20-2	20	2
21-2	21	2
22-2	22	2
23-2	23	2
24-2	24	2
1-3	1	3
2-3	2	3
3-3	3	3
4-3	4	3
5-3	5	3
6-3	6	3
7-3	7	3
8-3	8	3
9-3	9	3
10-3	10	3
11-3	11	3
12-3	12	3
13-3	13	3
14-3	14	3
15-3	15	3
16-3	16	3
17-3	17	3
18-3	18	3
19-3	19	3
20-3	20	3
21-3	21	3
22-3	22	3
23-3	23	3
24-3	24	3
1-4	1	4
2-4	2	4
3-4	3	4
4-4	4	4
5-4	5	4
6-4	6	4
7-4	7	4
8-4	8	4
9-4	9	4
10-4	10	4
11-4	11	4
12-4	12	4
13-4	13	4
14-4	14	4
15-4	15	4
16-4	16	4
17-4	17	4
18-4	18	4
19-4	19	4
20-4	20	4
21-4	21	4
22-4	22	4
23-4	23	4
24-4	24	4
1-5	1	5
2-5	2	5
3-5	3	5
4-5	4	5
5-5	5	5
6-5	6	5
7-5	7	5
8-5	8	5
9-5	9	5
10-5	10	5
11-5	11	5
12-5	12	5
13-5	13	5
14-5	14	5
15-5	15	5
16-5	16	5
17-5	17	5
18-5	18	5
19-5	19	5
20-5	20	5
21-5	21	5
22-5	22	5
23-5	23	5
24-5	24	5
1-6	1	6
2-6	2	6
3-6	3	6
4-6	4	6
5-6	5	6
6-6	6	6
7-6	7	6
8-6	8	6
9-6	9	6
10-6	10	6
11-6	11	6
12-6	12	6
13-6	13	6
14-6	14	6
15-6	15	6
16-6	16	6
17-6	17	6
18-6	18	6
19-6	19	6
20-6	20	6
21-6	21	6
22-6	22	6
23-6	23	6
24-6	24	6
0-0	0	0
\.


--
-- Data for Name: guitartab; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY guitartab (guitartab_id, finger1, finger2, finger3, finger4, thumb, barre, shape, chord) FROM stdin;
\.


--
-- Data for Name: guitartab_shape; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY guitartab_shape (guitartab_shape_id) FROM stdin;
C-Shape
D-Shape
E-Shape
G-Shape
A-Shape
Undefined
\.


--
-- Data for Name: note; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY note (note_id, value) FROM stdin;
1	C
2	C#/Dd
3	D
4	D#/Ed
5	E
6	F
7	F#/Gd
8	G
9	G#/Ad
10	A
11	A#/Bd
12	B
\.


--
-- Name: chord_ordering_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chord_ordering
    ADD CONSTRAINT chord_ordering_pkey PRIMARY KEY (chord_ordering_id);


--
-- Name: chord_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chord
    ADD CONSTRAINT chord_pkey PRIMARY KEY (chord_id);


--
-- Name: chord_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chord_type
    ADD CONSTRAINT chord_type_pkey PRIMARY KEY (chord_type_id);


--
-- Name: coordinate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY coordinate
    ADD CONSTRAINT coordinate_pkey PRIMARY KEY (coordinate_id);


--
-- Name: guitartab_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY guitartab
    ADD CONSTRAINT guitartab_pkey PRIMARY KEY (guitartab_id);


--
-- Name: guitartab_shape_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY guitartab_shape
    ADD CONSTRAINT guitartab_shape_pkey PRIMARY KEY (guitartab_shape_id);


--
-- Name: note_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY note
    ADD CONSTRAINT note_pkey PRIMARY KEY (note_id);


--
-- Name: guitartab_bef_trg; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER guitartab_bef_trg BEFORE INSERT OR UPDATE ON guitartab FOR EACH ROW EXECUTE PROCEDURE guitartab_bef_trg();


--
-- Name: barre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guitartab
    ADD CONSTRAINT barre_fkey FOREIGN KEY (barre) REFERENCES coordinate(coordinate_id);


--
-- Name: chord_chord_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY chord
    ADD CONSTRAINT chord_chord_type_id_fkey FOREIGN KEY (chord_type_id) REFERENCES chord_type(chord_type_id);


--
-- Name: chord_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guitartab
    ADD CONSTRAINT chord_fkey FOREIGN KEY (chord) REFERENCES chord(chord_id);


--
-- Name: finger1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guitartab
    ADD CONSTRAINT finger1_fkey FOREIGN KEY (finger1) REFERENCES coordinate(coordinate_id);


--
-- Name: finger2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guitartab
    ADD CONSTRAINT finger2_fkey FOREIGN KEY (finger2) REFERENCES coordinate(coordinate_id);


--
-- Name: finger3_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guitartab
    ADD CONSTRAINT finger3_fkey FOREIGN KEY (finger3) REFERENCES coordinate(coordinate_id);


--
-- Name: finger4_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guitartab
    ADD CONSTRAINT finger4_fkey FOREIGN KEY (finger4) REFERENCES coordinate(coordinate_id);


--
-- Name: shape_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guitartab
    ADD CONSTRAINT shape_fkey FOREIGN KEY (shape) REFERENCES guitartab_shape(guitartab_shape_id);


--
-- Name: thumb_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guitartab
    ADD CONSTRAINT thumb_fkey FOREIGN KEY (thumb) REFERENCES coordinate(coordinate_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

