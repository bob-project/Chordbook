### this file is mandatory to build the app in NixOS
{ghc}:
with (import <nixpkgs> {});

haskell.lib.buildStackProject {

	inherit ghc;
	name = "Chordbook";
	buildInputs = [ 
		postgresql_10
    haskellPackages.zlib
    zlib
    zlib.dev
	];

  shellHook = ''
    export PGDATA=$PWD/postgres_data
    export PGHOST=$PWD/postgres
    export POSTGRES_PORT="5432"
    export LOG_PATH=$PWD/postgres/LOG
    export PGDATABASE=postgres
    export DATABASE_URL="postgresql:///postgres?host=$PGHOST"
    if [ ! -d $PGHOST ]; then
      mkdir -p $PGHOST
    fi
    if [ ! -d $PGDATA ]; then
      echo 'Initializing postgresql database...'
      initdb $PGDATA --auth=trust >/dev/null
    fi
    pg_ctl start -l $LOG_PATH -o "-c listen_addresses= -c unix_socket_directories=$PGHOST"
 
    if ! $(psql -l | grep -q "chordbook"); then
      psql -f initDB.sql
      psql -f chordbook.sql
    fi

    pg_ctl stop
  '';
}
