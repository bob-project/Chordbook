{-# LANGUAGE 
	QuasiQuotes,
 	OverloadedStrings
#-}

module View
  ( start
    , route 
  ) where


import Data.Text as T
import Data.Text.Lazy as TL
import Text.Hamlet (Html, hamlet)
import Text.Julius (julius, renderJavascript, Javascript, JavascriptUrl, renderJavascriptUrl)
import Text.Blaze.Html.Renderer.String (renderHtml)


import Model
import Route
import HtmlTemplate
import GeneralStrings


----  Routing-Views ----
------------------------
start = do 
  renderHtml $ (getTemplate Standard Nothing Nothing)

route :: RoutingEnum -> Maybe GuitarTab -> String
route _ Nothing = do
  renderHtml $ (getTemplate Standard Nothing Nothing)

route Create (Just (GuitarTab guitartab_id finger1 finger2 finger3 finger4 thumb barre shape chord)) = do
  let successMsg = Just success_chord_create :: Maybe String
  renderHtml $ (getTemplate Standard successMsg Nothing)

route CreateError (Just (GuitarTab guitartab_id finger1 finger2 finger3 finger4 thumb barre shape chord)) = do
  let errorMsg = Just error_chord_create :: Maybe String
  renderHtml $ (getTemplate Standard Nothing errorMsg)

route Update (Just (GuitarTab guitartab_id finger1 finger2 finger3 finger4 thumb barre shape chord)) = do
  let successMsg = Just success_chord_update :: Maybe String
  renderHtml $ (getTemplate Standard successMsg Nothing)

route UpdateError (Just (GuitarTab guitartab_id finger1 finger2 finger3 finger4 thumb barre shape chord)) = do
  let errorMsg = Just error_chord_update :: Maybe String
  renderHtml $ (getTemplate Standard Nothing errorMsg)

route Delete (Just (GuitarTab guitartab_id finger1 finger2 finger3 finger4 thumb barre shape chord)) = do
  let successMsg = Just success_chord_delete :: Maybe String
  renderHtml $ (getTemplate Standard successMsg Nothing)

route DeleteError (Just (GuitarTab guitartab_id finger1 finger2 finger3 finger4 thumb barre shape chord)) = do
  let errorMsg = Just error_chord_delete :: Maybe String
  renderHtml $ (getTemplate Standard Nothing errorMsg)
------------------------
------------------------
