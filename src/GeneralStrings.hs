module GeneralStrings 
  ( success_chord_create
    , success_chord_update
    , success_chord_delete
    , error_chord_create
    , error_chord_update
    , error_chord_delete )
  where


----    Success     ----
------------------------
success_chord_create = "Chord successfully created"
success_chord_update = "Chord successfully updated"
success_chord_delete = "Chord successfully deleted"
------------------------
------------------------


----     Error      ----
------------------------
error_chord_create = "Chord creation failed!"
error_chord_update = "Chord update failed!"
error_chord_delete = "Chord deletion failed!"
------------------------
------------------------
