{-# LANGUAGE 
	OverloadedStrings
#-}

module Position
  (Position(..)
  , toPosition)
  where

import Data.Text.Lazy
import Data.Text.Lazy.Encoding

data Position = Position {
  x :: Int,
  y :: Int }
  deriving(Show, Eq)

class ToPosition x where
  toPosition :: x -> Maybe Position

instance ToPosition Text where
  toPosition x = position
    where
      splittedText = splitOn "-" x
      intList = Prelude.map textToInt splittedText
      position
        | (Prelude.length intList) == 2 =
          Just $ Position (Prelude.head intList) (Prelude.last intList)
        | otherwise = Nothing

textToInt :: Text -> Int
textToInt x = read (unpack x) :: Int
