{-# LANGUAGE 
	QuasiQuotes,
 	OverloadedStrings
#-}

module HtmlTemplate 
  ( getTemplate )
  where

import Text.Hamlet (Html, hamlet)
import Route

----  Html-Template ----
------------------------
getTemplate :: RoutingEnum -> Maybe String -> Maybe String -> Html
getTemplate Standard successParam dangerParam = [hamlet|
$doctype 5
<html>
    <head>
        <link rel="stylesheet" href="static/bootstrap.min.css">
        <link rel="stylesheet" href="static/guitarStyling.css">
        <script src="static/restClient.js">
        <title>Chordbook
    <body>
        <div class="container text-monospace">
          <h1>Chordbook
          <br>

          <div class="row"> 
            <div class="col-sm-9">
              <table id="fretboardTable" width="100%">
                <thead>
                  <tr>
                    <th>
                    <th>1
                    <th>2
                    <th>3
                    <th>4
                    <th>5
                <tbody>
                  <tr>
                    <th>E
                    <td>
                      <input id="check11" type="checkbox">
                      <label for="check11">.
                    <td>
                      <input id="check21" type="checkbox">
                      <label for="check21">.
                    <td>
                      <input id="check31" type="checkbox">
                      <label for="check31">.
                    <td>
                      <input id="check41" type="checkbox">
                      <label for="check41">.
                    <td>
                      <input id="check51" type="checkbox">
                      <label for="check51">.
                  <tr>
                    <th>A
                    <td>
                      <input id="check12" type="checkbox">
                      <label for="check12">.
                    <td>
                      <input id="check22" type="checkbox">
                      <label for="check22">.
                    <td>
                      <input id="check32" type="checkbox">
                      <label for="check32">.
                    <td>
                      <input id="check42" type="checkbox">
                      <label for="check42">.
                    <td>
                      <input id="check52" type="checkbox">
                      <label for="check52">.
                  <tr>
                    <th>D
                    <td>
                      <input id="check13" type="checkbox">
                      <label for="check13">.
                    <td>
                      <input id="check23" type="checkbox">
                      <label for="check23">.
                    <td>
                      <input id="check33" type="checkbox">
                      <label for="check33">.
                    <td>
                      <input id="check43" type="checkbox">
                      <label for="check43">.
                    <td>
                      <input id="check53" type="checkbox">
                      <label for="check53">.
                  <tr>
                    <th>G
                    <td>
                      <input id="check14" type="checkbox">
                      <label for="check14">.
                    <td>
                      <input id="check24" type="checkbox">
                      <label for="check24">.
                    <td>
                      <input id="check34" type="checkbox">
                      <label for="check34">.
                    <td>
                      <input id="check44" type="checkbox">
                      <label for="check44">.
                    <td>
                      <input id="check54" type="checkbox">
                      <label for="check54">.
                  <tr>
                    <th>B
                    <td>
                      <input id="check15" type="checkbox">
                      <label for="check15">.
                    <td>
                      <input id="check25" type="checkbox">
                      <label for="check25">.
                    <td>
                      <input id="check35" type="checkbox">
                      <label for="check35">.
                    <td>
                      <input id="check45" type="checkbox">
                      <label for="check45">.
                    <td>
                      <input id="check55" type="checkbox">
                      <label for="check55">.
                  <tr>
                    <th value="e">e
                    <td>
                      <input id="check16" type="checkbox">
                      <label for="check16">.
                    <td>
                      <input id="check26" type="checkbox">
                      <label for="check26">.
                    <td>
                      <input id="check36" type="checkbox">
                      <label for="check36">.
                    <td>
                      <input id="check46" type="checkbox">
                      <label for="check46">.
                    <td>
                      <input id="check56" type="checkbox">
                      <label for="check56">.
  
  
            <div class="col-3">
              <select class="custom-select" id="guitarTabComboBox" onchange="reactOnChange();">
                <option id="blank">

              <br>
              <label>
              <input class="btn btn-success btn-block" id="sendPostCreate" type="button" value="Save" onClick="sendPostCreate();">
              <input class="btn btn-info btn-block" id="sendPutUpdate" type="button" value="Update" onClick="sendPutUpdate();" disabled>
              <input class="btn btn-danger btn-block" id="sendDelete" type="button" value="Delete" onClick="sendDelete();" disabled>
          
          $maybe successMsg <- successParam
            <br>
            <div class="alert alert-success" role="alert">
              <h4 class="alert-heading"> Success
              <p> #{successMsg}

          $maybe dangerMsg <- dangerParam
            <br>
            <div class="alert alert-danger" role="alert">
              <h4 class="alert-heading"> Error
              <p> #{dangerMsg}
|] undefined -- is undefined because we use shakespeare standalone
------------------------
------------------------
