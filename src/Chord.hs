{-# LANGUAGE 
	OverloadedStrings
#-}

-- TODO Expose this functions to the TestLoader instead of opening them to the public in here
module Chord
  (Chord(..)
  , Note(..)
  , toChord
  , toNote
  , createPositionList
  , filterPositionsByY
  , orderPositionsByLowestY
  , createOpenPositions
  , openPositionsList
  , transformOrderedPositionsToChord
  , convertGuitarTabToChord)
  where

import Prelude
import Data.List
import Data.Text.Lazy
import Data.Text.Lazy.Encoding

import Model
import Position

data Chord =
  Chord { chord_id :: Maybe Text,
    tabshape_chord_id :: Maybe Text, -- one chord can have multiple shapes
    note_1 :: Text,
    note_2 :: Text,
    note_3 :: Text,
    note_4 :: Text,
    note_5 :: Text,
    note_6 :: Text,
    note_7 :: Text,
    note_8 :: Text,
    note_9 :: Text,
    note_10 :: Text,
    numberOfNotes :: Text,
    inversion :: Text,
    scaleDegree :: Text,
    symbol :: Text } -- should be a primary key on the database
  deriving(Show, Eq)

class ToChord x where
  toChord :: x -> Chord

instance ToChord GuitarTab where
  toChord x = convertGuitarTabToChord x


convertGuitarTabToChord :: GuitarTab -> Chord
convertGuitarTabToChord x |
  (finger1 x) == "" &&
  (finger2 x) == "" &&
  (finger3 x) == "" &&
  (finger4 x) == "" &&
  (thumb x) == Nothing = (
  -- TODO (barre x) == Nothing -- Add barre to createPositionlist
    Chord
      Nothing
      Nothing
      "E" 
      "A"
      "D"
      "G"
      "B"
      ""
      ""
      ""
      ""
      ""
      "5"
      "inversion"
      "scaleDegree"
      "symbol")
  | otherwise = transformOrderedPositionsToChord orderedList
  where
    positionList = createPositionList x
    filteredList = filterPositionsByY positionList
    updatedWithOpenChordsList = (createOpenPositions filteredList openPositionsList) ++ filteredList
    orderedList = orderPositionsByLowestY updatedWithOpenChordsList
    
openPositionsList = [
  Position 0 1,
  Position 0 2,
  Position 0 3,
  Position 0 4,
  Position 0 5,
  Position 0 6 ]

createPositionList :: GuitarTab -> [Position]
createPositionList x = addToPositionList (finger1 x)
  ++ addToPositionList (finger2 x)
  ++ addToPositionList (finger3 x)
  ++ addToPositionList (finger4 x)
  ++ addToPositionList unpackedThumb
  where
    unpackedThumb = case (thumb x) of
      Nothing -> "" :: Text
      Just thumb -> thumb

addToPositionList :: Text -> [Position]
addToPositionList x = case (toPosition x) of
  Nothing -> []
  Just x -> x : []

filterPositionsByY :: [Position] -> [Position]
filterPositionsByY xs = Prelude.foldl findYAndDecide xs []

findYAndDecide :: [Position] -> Position -> [Position]
findYAndDecide xs x
  | (isSameYPresent xs x) = (replaceSmallerX xs x)
  | otherwise = xs

isSameYPresent :: [Position] -> Position -> Bool
isSameYPresent (curElem:xs) searchValue
  | (y curElem) == (y searchValue) = True
  | otherwise = isSameYPresent xs searchValue 
isSameYPresent _ _ = False

replaceSmallerX :: [Position] -> Position -> [Position]
replaceSmallerX (indexValue:xs) searchValue
  | Data.List.null xs = []
  | (y indexValue) == (y searchValue) &&
    (x indexValue) < (x searchValue) = searchValue : nextRound
  | otherwise = indexValue : nextRound
  where
    nextRound = replaceSmallerX xs searchValue

createOpenPositions :: [Position] -> [Position] -> [Position]
createOpenPositions xs (position : openPositions)
  | (isSameYPresent xs position) == False = position : modifiedPositions
  | otherwise = modifiedPositions
  where
    modifiedPositions = createOpenPositions xs openPositions

createOpenPositions _ xs = xs
 

orderPositionsByLowestY xs = sortBy orderByLowestY xs
orderByLowestY :: Position -> Position -> Ordering
orderByLowestY a b
  | (y a) > (y b) = GT
  | (y a) < (y b) = LT
  | (y a) == (y b) = EQ


-- TODO Move this to an distinct class
data Note = A
  | AB
  | B
  | C
  | CD
  | D
  | DE
  | E
  | F
  | FG
  | G
  | GA
  deriving(Show, Eq)
  
-- TODO move to seperate class
class ToString x where
  toString :: x -> String

instance ToString Note where
  toString x = case x of 
    A -> "A" 
    AB -> "A#/Bd"
    B -> "B"
    C -> "C"
    CD -> "C#/Dd"
    D -> "D"
    DE -> "D#/Ed"
    E -> "E"
    F -> "F"
    FG -> "F#/Gd"
    G -> "G"
    GA -> "G#/Ad"

-- TODO instead of using {}, use an init function for creating a chord
transformOrderedPositionsToChord :: [Position] -> Chord
transformOrderedPositionsToChord xs = mapPositionToNoteAndAddToChord xs createEmptyChord

createEmptyChord :: Chord
createEmptyChord = Chord
      Nothing
      Nothing
      "" 
      ""
      ""
      ""
      ""
      ""
      ""
      ""
      ""
      ""
      "0"
      ""
      ""
      ""

mapPositionToNoteAndAddToChord :: [Position] -> Chord -> Chord
mapPositionToNoteAndAddToChord (x:xs) chord
  | otherwise = mapPositionToNoteAndAddToChord xs (updateNextFreeNote x chord)

mapPositionToNoteAndAddToChord _ chord = chord

updateNextFreeNote :: Position -> Chord -> Chord
updateNextFreeNote x chord
  | note_1 chord == "" = chord { note_1 = newNote, numberOfNotes = updatedNoteCount }
  | note_2 chord == "" = chord { note_2 = newNote, numberOfNotes = updatedNoteCount }
  | note_3 chord == "" = chord { note_3 = newNote, numberOfNotes = updatedNoteCount }
  | note_4 chord == "" = chord { note_4 = newNote, numberOfNotes = updatedNoteCount }
  | note_5 chord == "" = chord { note_5 = newNote, numberOfNotes = updatedNoteCount }
  | note_6 chord == "" = chord { note_6 = newNote, numberOfNotes = updatedNoteCount }
  | note_7 chord == "" = chord { note_7 = newNote, numberOfNotes = updatedNoteCount }
  | note_8 chord == "" = chord { note_8 = newNote, numberOfNotes = updatedNoteCount }
  | note_9 chord == "" = chord { note_9 = newNote, numberOfNotes = updatedNoteCount }
  | note_10 chord == "" = chord { note_10 = newNote, numberOfNotes = updatedNoteCount}
  | otherwise = chord
  where
    realNumber = read $ unpack $ numberOfNotes chord :: Int
    incrementedNumber = realNumber + 1
    updatedNoteCount = pack $ show incrementedNumber
    newNote = pack $ toString $ toNote x

class ToNote x where
  toNote :: x -> Note

instance ToNote Position where
  toNote position = toNote generalOffset
    where
      xPos = (x position)
      yPos = (y position)
      individualOffset
        | yPos == 6 = 7 + xPos -- E
        | yPos == 2 = 0 + xPos -- A
        | yPos == 3 = 5 + xPos -- D
        | yPos == 4 = 10 + xPos -- G
        | yPos == 5 = 2 + xPos -- B
        | yPos == 1 = 7 + xPos -- E
      generalOffset
        | individualOffset >= 12 = individualOffset `mod` 12
        | otherwise = individualOffset

instance ToNote Int where
  toNote integer
    | integer == 0 = A
    | integer == 1 = AB
    | integer == 2 = B
    | integer == 3 = C
    | integer == 4 = CD
    | integer == 5 = D
    | integer == 6 = DE
    | integer == 7 = E
    | integer == 8 = F
    | integer == 9 = FG
    | integer == 10 = G
    | integer == 11 = GA

-- TODO 
-- write function to determine:
--   - inversion
--   - scaleDegree
--   - symbol
