{-# LANGUAGE OverloadedStrings #-}

module Controller
  ( select_GuitarTabs
  , create_GuitarTab )
  where

import Control.Monad.IO.Class
import Control.Applicative
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Web.Scotty.Internal.Types (ActionT)
import Data.Text
import qualified Data.Text.Lazy as TL
import GHC.Int


import Model


----   Connection   ----
------------------------
localConnection :: IO Connection
localConnection = connectPostgreSQL "host='localhost' port='5432' dbname='chordbook' user='bob' password='bob'"
------------------------
------------------------


----    Queries     ----
------------------------
select_GuitarTabs_Query :: Query
select_GuitarTabs_Query = "select * from guitartab"

insert_GuitarTab_Query :: Query
--insert_GuitarTab_Query =  "insert into guitartab (finger1, finger2, finger3, finger4) values (?, ?, ?, ?)"
insert_GuitarTab_Query =  "insert into guitartab (guitartab_id, finger1, finger2, finger3, finger4, thumb, barre, shape, chord) values (?, ?, ?, ?, ?, ?, ?, ?, ?)"
------------------------
------------------------


----    Wrappers    ----
------------------------
-- Select
select_GuitarTabs :: IO [GuitarTab]
select_GuitarTabs = db_retrieve select_GuitarTabs_Query


-- Insert
create_GuitarTab :: Maybe GuitarTab -> ActionT TL.Text IO Int64
create_GuitarTab guitarTab = do
  liftIO $ insert_GuitarTab guitarTab

insert_GuitarTab :: Maybe GuitarTab -> IO Int64
insert_GuitarTab guitarTab = case guitarTab of
  Just guitarTab -> (db_execute insert_GuitarTab_Query guitarTab)
  Nothing -> return (0 :: Int64)
------------------------
------------------------


----    DB-Driver   ----
------------------------
db_retrieve :: Query -> IO [GuitarTab]
db_retrieve query = do
  conn <- localConnection
  xs <- query_ conn query
  return xs

db_execute :: Query -> GuitarTab -> IO Int64
db_execute query chord = do
  conn <- localConnection
  (execute conn query chord)
------------------------
------------------------
