{-# LANGUAGE 
	OverloadedStrings,
	DeriveGeneric 
#-}

module Route 
  ( RoutingEnum(..) )
  where


----     Routes     ----
------------------------
data RoutingEnum = Standard
  | Create 
  | CreateError 
  | Update
  | UpdateError 
  | Delete
  | DeleteError
  deriving (Enum)
------------------------
------------------------
