{-# LANGUAGE 
	OverloadedStrings,
	DeriveGeneric 
#-}

-- TODO Rename this Model to GuitarTab
module Model 
  ( GuitarTab(..)
  , FromJSON )
  where

import Data.Text.Lazy
import Data.Text.Lazy.Encoding
import Data.Aeson
import Control.Applicative
import GHC.Generics
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToRow


data GuitarTab = 
  GuitarTab { guitartab_id :: Maybe Text,
    finger1 :: Text,
    finger2 :: Text,
    finger3 :: Text,
    finger4 :: Text,
    thumb :: Maybe Text,
    barre :: Maybe Text,
    shape :: Maybe Text,
    chord :: Maybe Text }
  deriving(Show, Generic)

-- Aeson
instance FromJSON GuitarTab
instance ToJSON GuitarTab

-- Postgres-Simple
instance FromRow GuitarTab 
instance ToRow GuitarTab 
