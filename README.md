### Chordbook
A digital Chordbook for memorizing Chords.

![Chordbook_Screenshot](Chordbook_Screenshot.png)

### How to build and run on NixOS:
- download the source
- extract and navigate into the source-directoy
- stack setup
- stack build
- check if database "chordbook" was created by shell.nix
- activate postgres-service on port 5432 in /etc/nixos/configuration.nix
- stack exec Chordbook-exe
- go to "localhost:3000/" in your browser

### How to start Unit-Tests
- stack build
- stack test

### Nix Package (In Progress) - https://github.com/rschardt/nixpkgs/commits/master
- nix-env -iA nixos.chordbook
- go to "localhost:3000/" in your browser

### Technical-Stack:
- PostgresSQL
- Postgres-Simple
- Haskell
- Aeson 
- Shakespeare 
- REST-Server per Scotty
- REST-Client per Javascript
- Bootstrap

### Coding-Principles:
- Clean Code
- MVC
- TDD

### Goal:
First of all, this is an one person hobby-project.
I wanted to practise music-theory, haskell and REST by writing this application.

It would be great when this evolves into an actual application for people who want to train and memorize their Chord-Knowledge.

There is still a lot to implement but the GUI will not change much.

### Bugs and TODOs can be found in Chordbook.org

Copyright (c) 2018 Robert Schardt 
