CREATE ROLE bob; 
alter role bob with password 'bob'; 
alter role bob login; 
alter role bob SUPERUSER;

create database chordbook; 
